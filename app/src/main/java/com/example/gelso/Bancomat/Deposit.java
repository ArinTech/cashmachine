package com.example.gelso.Bancomat;

import android.app.NotificationManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.gelso.bank.R;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class Deposit extends AppCompatActivity {

    Button btn_deposit,btn_cancel;
    EditText txt_money_deposit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);
        
        //findViewByIds
        layout_setting();



        btn_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Integer.parseInt(txt_money_deposit.getText().toString()) > 10) {

                    Intent i = getIntent();
                    i.putExtra("money", txt_money_deposit.getText().toString());
                    setResult(RESULT_OK,i);
                    finish();
                }else{
                    //PUSH NOTIFICATION
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.notification_icon)
                                    .setContentTitle("Cache Machine")
                                    .setContentText("minimun 10� !!! ");
                    NotificationManager mNotificationManager =(NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                    mNotificationManager.notify(0, mBuilder.build());
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }

    public void layout_setting() {
        //BTN DEPOSIT
        btn_deposit = (Button) findViewById(R.id.btn_deposit);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        txt_money_deposit = (EditText) findViewById(R.id.txt_money_deposit);
    }
}

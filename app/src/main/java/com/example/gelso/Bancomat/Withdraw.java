package com.example.gelso.Bancomat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gelso.bank.R;

public class Withdraw extends AppCompatActivity {

    Button btn_cancel_Withdraw;
    Button btn_ok_Withdraw;
    EditText withdraw_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        //FIND VIEW BY ID
        btn_cancel_Withdraw = (Button) findViewById(R.id.btn_cancel_Withdraw);
        btn_ok_Withdraw = (Button) findViewById(R.id.btn_ok_Withdraw);
        withdraw_text = (EditText) findViewById(R.id.withdraw_text);


        //WITHDRAW BUTTON
        btn_ok_Withdraw.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                Intent i = getIntent();
                Toast.makeText(getApplicationContext(),withdraw_text.getText().toString(), Toast.LENGTH_SHORT).show();
                i.putExtra("withdraw", withdraw_text.getText().toString());
                setResult(RESULT_OK,i);
                finish();

            }
        });



        //CANCEL BUTTON
        btn_cancel_Withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = getIntent();
                setResult(RESULT_CANCELED,i);
                finish();
            }
        });


        }


}


